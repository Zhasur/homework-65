import axios from 'axios'

const instance = axios.create({
    baseURL: 'https://burger-project-js.firebaseio.com/'
});

export default instance