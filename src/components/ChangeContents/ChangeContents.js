import React, {Component} from 'react';
import axios from '../../axios-pages';
import {FormGroup, Input} from 'reactstrap'
import Spinner from "../UI/Spinner";

import './ChangeContents.css'

class ChangeContents extends Component {
    state = {
        pages: null,
        page: '',
        title: '',
        content: '',
        loading: true,

    };

    componentDidMount() {
        axios.get('pages.json').then(response => {
            let pagesName = Object.keys(response.data);
            this.setState({pages: pagesName, page: pagesName[0], loading:false})
        })

    }

    changePages = event => {
        const page = event.target.name;
        this.setState({[page]: event.target.value})
    };

    componentDidUpdate(prevProps, prevState) {
        if (this.state.page !== prevState.page) {
            axios.get('pages/' + this.state.page + '.json').then(response => {
                this.setState({title: response.data.title, content: response.data.content})
            })
        }
    };

    saveChanged = (event) => {
        event.preventDefault();

        let change = {
            title: this.state.title,
            content: this.state.content,
        };

        axios.put('pages/' + this.state.page + '.json', change).then(() => {
            this.props.history.push('/pages/' + this.state.page)

        })
    };


    render() {
        if (this.state.loading) {
            return  <Spinner/>
        }

        return (
            <div className="change-block">
                <form onSubmit={this.saveChanged}>
                    <FormGroup>
                        <label className="label">Title</label>
                        <input className="input" value={this.state.title} onChange={this.changePages} name="title"/>
                    </FormGroup>
                    <FormGroup>
                        <label className="label">Content</label>
                        <input className="input" value={this.state.content} onChange={this.changePages} name="content"/>
                    </FormGroup>
                    <FormGroup>
                        <label className="label">Select</label>
                        <select className="input select" name="page" id="page"
                               onChange={this.changePages}
                               value={this.state.page}
                        >
                            {this.state.pages.map((page, key) => (
                                <option key={key} >{page}</option>
                            ))}
                        </select>
                    </FormGroup>
                    <FormGroup>
                        <button className="save-btn" color="info">Save</button>
                    </FormGroup>

                </form>
            </div>
        );
    }
}

export default ChangeContents;