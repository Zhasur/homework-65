import React from 'react';

import './Logo.css'
import LogoImg from '../../assets/images/logo.png'
import NavigationItem from "../Navigation/NavigationItems/NavigationItem/NavigationItem";

const Logo = () => (
    <div className="Logo">
        <NavigationItem to="/pages/about"><img src={LogoImg} className="logo" alt="MyBurger"/></NavigationItem>
    </div>
);

export default Logo;