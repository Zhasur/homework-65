import React from 'react';

import './NavigationItems.css'
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/pages/about" exact>About</NavigationItem>
        <NavigationItem to="/pages/products" exact>Products</NavigationItem>
        <NavigationItem to="/pages/contacts" exact>Contacts</NavigationItem>
        <NavigationItem to="/pages/history" exact>History</NavigationItem>
        <NavigationItem to="/pages/clients" exact>Clients</NavigationItem>
        <NavigationItem to="/admin" exact>Admin</NavigationItem>
    </ul>
);

export default NavigationItems;