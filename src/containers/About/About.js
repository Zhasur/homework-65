import React, {Component} from 'react';

import axios from '../../axios-pages'
import Spinner from "../../components/UI/Spinner";

class About extends Component {
    state = {
        page: null,
        loading: true,
    };

    componentDidMount() {
        axios.get('pages/' + this.props.match.params.name + '.json').then(response => {
            this.setState({page: response.data})
        }).finally(() => {
            this.setState({loading: false})
        })
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.name !== prevProps.match.params.name) {
            axios.get('pages/' + this.props.match.params.name + '.json').then(response => {
                console.log(response.data);
                this.setState({page: response.data})
            })
        }
    }

    render() {
        if (this.state.loading){
            return <Spinner/>;
        }

        return (
            <div className="main-block">
                <h1 className="title-about" style={{textAlign: 'center'}}>{this.state.page.title}</h1>
                <p className="text-about" style={{textAlign: 'center'}}>{this.state.page.content}</p>
            </div>
        );
    }
}

export default About;