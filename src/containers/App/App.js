import React, { Component } from 'react';
import {Switch, Route} from "react-router-dom";
import Layout from "../../components/Layout/Layout";
import About from "../About/About";
import ChangeContents from "../../components/ChangeContents/ChangeContents";

class App extends Component {
    render() {
        return (
            <Layout>
                <Switch>
                    <Route path="/pages/:name" component={About} />
                    <Route path="/admin" component={ChangeContents} />
                </Switch>
            </Layout>
        );
    }
}

export default App;
